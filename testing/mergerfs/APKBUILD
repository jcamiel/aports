# Contributor: Nathan Caldwell <saintdev@gmail.com>
# Maintainer: Nathan Caldwell <saintdev@gmail.com>
# NOTE: For now we must use vendored libfuse, see the URL below for details.
#       https://github.com/trapexit/mergerfs/blob/2.27.0/README.md#why-was-libfuse-embedded-into-mergerfs
pkgname=mergerfs
pkgver=2.36.0
pkgrel=0
pkgdesc="A FUSE based union filesystem"
url="https://github.com/trapexit/mergerfs"
arch="all"
license="ISC"
depends="libattr"
makedepends="autoconf automake libtool gettext-dev attr-dev linux-headers"
options="suid !check" # No test suite
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/trapexit/mergerfs/archive/$pkgver.tar.gz"

prepare() {
	default_prepare
	echo "$pkgver" > "$builddir"/VERSION
}

build() {
	export GIT_CEILING_DIRECTORIES="$srcdir"
	make OPTS="$CXXFLAGS"
}

package() {
	make PREFIX="/usr" DESTDIR="$pkgdir" install
}

sha512sums="
f20879aa5fb9ab56b1e7bba0e08aacda7e26af7e1fdcf3d57b77a2dce70fda8aa15c20e947dde6ffab3dfae8698005c9d8383c9ab2402cc4ce5de0810ce1298d  mergerfs-2.36.0.tar.gz
"
