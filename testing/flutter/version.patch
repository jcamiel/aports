--- a/packages/flutter_tools/lib/src/version.dart
+++ b/packages/flutter_tools/lib/src/version.dart
@@ -76,13 +76,9 @@
     String? frameworkRevision,
   }) : _clock = clock,
        _workingDirectory = workingDirectory {
-    _frameworkRevision = frameworkRevision ?? _runGit(
-      gitLog(<String>['-n', '1', '--pretty=format:%H']).join(' '),
-      globals.processUtils,
-      _workingDirectory,
-    );
+    _frameworkRevision = frameworkRevision ?? 'alpineaports0000000000000000000000000000';
     _gitTagVersion = GitTagVersion.determine(globals.processUtils, globals.platform, workingDirectory: _workingDirectory, gitRef: _frameworkRevision);
-    _frameworkVersion = gitTagVersion.frameworkVersionFor(_frameworkRevision);
+    _frameworkVersion = globals.fs.file(globals.fs.path.join(Cache.flutterRoot!, 'version')).readAsStringSync();
   }
 
   final SystemClock _clock;
@@ -95,7 +95,7 @@
     _frameworkVersion = gitTagVersion.frameworkVersionFor(_frameworkRevision);
   }
 
-  String? _repositoryUrl;
+  final String _repositoryUrl = 'https://pkgs.alpinelinux.org/packages?name=flutter';
   String? get repositoryUrl {
     final String _ = channel;
     return _repositoryUrl;
@@ -111,25 +107,8 @@
   String get channel {
     String? channel = _channel;
     if (channel == null) {
-      final String gitChannel = _runGit(
-        'git rev-parse --abbrev-ref --symbolic $kGitTrackingUpstream',
-        globals.processUtils,
-        _workingDirectory,
-      );
-      final int slash = gitChannel.indexOf('/');
-      if (slash != -1) {
-        final String remote = gitChannel.substring(0, slash);
-        _repositoryUrl = _runGit(
-          'git ls-remote --get-url $remote',
-          globals.processUtils,
-          _workingDirectory,
-        );
-        channel = gitChannel.substring(slash + 1);
-      } else if (gitChannel.isEmpty) {
-        channel = 'unknown';
-      } else {
-        channel = gitChannel;
-      }
+      // we only package stable
+      channel = 'stable';
       _channel = channel;
     }
     return channel;
@@ -148,11 +127,7 @@
 
   String? _frameworkAge;
   String get frameworkAge {
-    return _frameworkAge ??= _runGit(
-      gitLog(<String>['-n', '1', '--pretty=format:%ar']).join(' '),
-      globals.processUtils,
-      _workingDirectory,
-    );
+    return _frameworkAge ??= 'unknown (alpine package)';
   }
 
   late String _frameworkVersion;
@@ -166,7 +141,6 @@
   String get engineRevisionShort => _shortGitRevision(engineRevision);
 
   void ensureVersionFile() {
-    globals.fs.file(globals.fs.path.join(Cache.flutterRoot!, 'version')).writeAsStringSync(_frameworkVersion);
   }
 
   @override
@@ -212,28 +186,9 @@
     String gitRef = 'HEAD',
     bool lenient = false,
   }) {
-    final List<String> args = gitLog(<String>[
-      gitRef,
-      '-n',
-      '1',
-      '--pretty=format:%ad',
-      '--date=iso',
-    ]);
-    try {
-      // Don't plumb 'lenient' through directly so that we can print an error
-      // if something goes wrong.
-      return _runSync(args, lenient: false);
-    } on VersionCheckError catch (e) {
-      if (lenient) {
-        final DateTime dummyDate = DateTime.fromMillisecondsSinceEpoch(0);
-        globals.printError('Failed to find the latest git commit date: $e\n'
-          'Returning $dummyDate instead.');
-        // Return something that DateTime.parse() can parse.
-        return dummyDate.toString();
-      } else {
-        rethrow;
-      }
-    }
+    final DateTime dummyDate = DateTime.fromMillisecondsSinceEpoch(0);
+    // Return something that DateTime.parse() can parse.
+    return dummyDate.toString();
   }
 
   /// Checks if the currently installed version of Flutter is up-to-date, and
@@ -276,8 +231,6 @@
   /// remote git repository is not reachable due to a network issue.
   static Future<String> fetchRemoteFrameworkCommitDate() async {
     try {
-      // Fetch upstream branch's commit and tags
-      await _run(<String>['git', 'fetch', '--tags']);
       return _gitCommitDate(gitRef: kGitTrackingUpstream);
     } on VersionCheckError catch (error) {
       globals.printError(error.message);
@@ -299,7 +252,7 @@
   /// the branch name will be returned as `'[user-branch]'`.
   String getBranchName({ bool redactUnknownBranches = false }) {
     _branch ??= () {
-      final String branch = _runGit('git rev-parse --abbrev-ref HEAD', globals.processUtils);
+      const String branch = 'stable';
       return branch == 'HEAD' ? channel : branch;
     }();
     if (redactUnknownBranches || _branch!.isEmpty) {
@@ -410,49 +363,7 @@
   ///
   /// Returns [VersionCheckError] if the tracking remote is not standard.
   VersionCheckError? run(){
-    final String? flutterGit = platform.environment['FLUTTER_GIT_URL'];
-    final String? repositoryUrl = version.repositoryUrl;
-
-    if (repositoryUrl == null) {
-      return VersionCheckError(
-        'The tool could not determine the remote upstream which is being '
-        'tracked by the SDK.'
-      );
-    }
-
-    // Strip `.git` suffix before comparing the remotes
-    final List<String> sanitizedStandardRemotes = <String>[
-      // If `FLUTTER_GIT_URL` is set, use that as standard remote.
-      if (flutterGit != null) flutterGit
-      // Else use the predefined standard remotes.
-      else ..._standardRemotes,
-    ].map((String remote) => stripDotGit(remote)).toList();
-
-    final String sanitizedRepositoryUrl = stripDotGit(repositoryUrl);
-
-    if (!sanitizedStandardRemotes.contains(sanitizedRepositoryUrl)) {
-      if (flutterGit != null) {
-        // If `FLUTTER_GIT_URL` is set, inform to either remove the
-        // `FLUTTER_GIT_URL` environment variable or set it to the current
-        // tracking remote.
-        return VersionCheckError(
-          'The Flutter SDK is tracking "$repositoryUrl" but "FLUTTER_GIT_URL" '
-          'is set to "$flutterGit".\n'
-          'Either remove "FLUTTER_GIT_URL" from the environment or set it to '
-          '"$repositoryUrl". '
-          'If this is intentional, it is recommended to use "git" directly to '
-          'manage the SDK.'
-        );
-      }
-      // If `FLUTTER_GIT_URL` is unset, inform to set the environment variable.
-      return VersionCheckError(
-        'The Flutter SDK is tracking a non-standard remote "$repositoryUrl".\n'
-        'Set the environment variable "FLUTTER_GIT_URL" to '
-        '"$repositoryUrl". '
-        'If this is intentional, it is recommended to use "git" directly to '
-        'manage the SDK.'
-      );
-    }
+    // the worse shit any code says about us, the worse for that code.
     return null;
   }
 
@@ -620,10 +531,7 @@
 }
 
 String _runGit(String command, ProcessUtils processUtils, [String? workingDirectory]) {
-  return processUtils.runSync(
-    command.split(' '),
-    workingDirectory: workingDirectory ?? Cache.flutterRoot,
-  ).stdout.trim();
+  return '';
 }
 
 /// Runs [command] in the root of the Flutter installation and returns the
@@ -647,7 +555,7 @@
   if (revision == null) {
     return '';
   }
-  return revision.length > 10 ? revision.substring(0, 10) : revision;
+  return revision.length > 12 ? revision.substring(0, 12) : revision;
 }
 
 /// Version of Flutter SDK parsed from Git.
@@ -708,42 +616,10 @@
     bool fetchTags = false,
     String gitRef = 'HEAD'
   }) {
-    if (fetchTags) {
-      final String channel = _runGit('git rev-parse --abbrev-ref HEAD', processUtils, workingDirectory);
-      if (channel == 'dev' || channel == 'beta' || channel == 'stable') {
-        globals.printTrace('Skipping request to fetchTags - on well known channel $channel.');
-      } else {
-        final String flutterGit = platform.environment['FLUTTER_GIT_URL'] ?? 'https://github.com/flutter/flutter.git';
-        _runGit('git fetch $flutterGit --tags -f', processUtils, workingDirectory);
-      }
-    }
-    // find all tags attached to the given [gitRef]
-    final List<String> tags = _runGit(
-      'git tag --points-at $gitRef', processUtils, workingDirectory).trim().split('\n');
-
-    // Check first for a stable tag
-    final RegExp stableTagPattern = RegExp(r'^\d+\.\d+\.\d+$');
-    for (final String tag in tags) {
-      if (stableTagPattern.hasMatch(tag.trim())) {
-        return parse(tag);
-      }
-    }
-    // Next check for a dev tag
-    final RegExp devTagPattern = RegExp(r'^\d+\.\d+\.\d+-\d+\.\d+\.pre$');
-    for (final String tag in tags) {
-      if (devTagPattern.hasMatch(tag.trim())) {
-        return parse(tag);
-      }
-    }
-
     // If we're not currently on a tag, use git describe to find the most
     // recent tag and number of commits past.
     return parse(
-      _runGit(
-        'git describe --match *.*.* --long --tags $gitRef',
-        processUtils,
-        workingDirectory,
-      )
+      globals.fs.file(globals.fs.path.join(Cache.flutterRoot!, 'version')).readAsStringSync()
     );
   }

