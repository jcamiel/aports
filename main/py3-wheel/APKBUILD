# Contributor: Antoine Fontaine <antoine.fontaine@epfl.ch>
# Maintainer: Peter Shkenev <santurysim@gmail.com>
pkgname=py3-wheel
pkgver=0.41.0
pkgrel=2
pkgdesc="built-package format for Python"
url="https://github.com/pypa/wheel"
arch="noarch"
license="MIT"
depends="python3 py3-packaging"
makedepends="
	py3-flit-core
	py3-gpep517
	"
checkdepends="py3-pytest py3-setuptools python3-dev"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/w/wheel/wheel-$pkgver.tar.gz
	use-system-packaging.patch
	"
builddir="$srcdir"/wheel-$pkgver
options="!check" # circular with pytest

prepare() {
	default_prepare

	rm -rf src/wheel/vendored
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages test-env
	test-env/bin/python3 setup.py install
	test-env/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
2e5099f5d512dd8e2205cfefa68e9229a7713367ec367f580021eeb68fb5bc0382bd89a524aa8f0d7617eb26f5d57eaa5e0976d94d042a6ffa20738624d3bae7  wheel-0.41.0.tar.gz
3ff5bb40767039ced5073c18a5c8e069387997f0179709e93f4f90ffc41933037b61a62728702e1a45053f2575e056a8afcf0cce972f91e97a38a6dfa7aa153d  use-system-packaging.patch
"
