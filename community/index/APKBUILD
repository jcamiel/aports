# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=maui
pkgname=index
pkgver=2.2.1
pkgrel=1
pkgdesc="Index file manager based on MauiKit"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://invent.kde.org/maui/index-fm"
license="GPL-2.0-or-later"
depends="mauikit-imagetools"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kio-dev
	knotifications-dev
	kservice-dev
	mauikit-dev
	mauikit-filebrowsing-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"
source="https://download.kde.org/stable/maui/index/$pkgver/index-fm-$pkgver.tar.xz"
options="!check" # No tests available
builddir="$srcdir/$pkgname-fm-$pkgver"

_commit=""
_repo_url="https://invent.kde.org/maui/index-fm.git"
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 $_repo_url .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
da1d4def03b49d0cc208d60e9984beee4fe9c3406a87acd0f720b577ccd58a24c357e4a4ef53c7dcd8b9395e8efb5468d4453b58211ef612399eaf03bdc48312  index-fm-2.2.1.tar.xz
"
