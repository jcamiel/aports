# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Pierre-Gilas MILLON <pgmillon@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=libgit2
pkgver=1.7.0
pkgrel=1
pkgdesc="linkable library for Git"
url="https://libgit2.org/"
arch="all"
license="GPL-2.0-only WITH GCC-exception-2.0"
depends_dev="curl-dev libssh2-dev http-parser-dev"
makedepends_host="$depends_dev zlib-dev openssl-dev>3 pcre2-dev"
makedepends_build="python3 cmake samurai"
subpackages="$pkgname-dbg $pkgname-static $pkgname-dev $pkgname-tools $pkgname-tests::noarch"
options="net"
source="$pkgname-$pkgver.tar.gz::https://github.com/libgit2/libgit2/archive/v$pkgver.tar.gz
	$pkgname-fix-grafts.patch::https://github.com/libgit2/libgit2/commit/9d4c550564ee254dda9e2620c4c1e32ebb529728.diff
	fix-util-tests.patch
	missing-header.patch
	no-online-test.patch
	"

# secfixes:
#   1.4.4-r0:
#     - CVE-2022-29187
#     - CVE-2022-24765
#   0.28.4-r0:
#     - CVE-2019-1348
#     - CVE-2019-1349
#     - CVE-2019-1350
#     - CVE-2019-1351
#     - CVE-2019-1352
#     - CVE-2019-1353
#     - CVE-2019-1354
#     - CVE-2019-1387
#   0.27.4-r0:
#     - CVE-2018-15501
#   0.27.3-r0:
#     - CVE-2018-10887
#     - CVE-2018-10888
#   0.25.1-r0:
#     - CVE-2016-10128
#     - CVE-2016-10129
#     - CVE-2016-10130
#   0.24.3-r0:
#     - CVE-2016-8568
#     - CVE-2016-8569

build() {
	CFLAGS="$CFLAGS -flto=auto" \
	cmake -B build-shared -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DUSE_HTTP_PARSER=system \
		-DUSE_HTTPS=OpenSSL \
		-DREGEX_BACKEND=pcre2 \
		-DUSE_BUNDLED_ZLIB=OFF \
		-DUSE_SSH=ON
	cmake --build build-shared

	cmake -B build-static -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DUSE_HTTP_PARSER=system \
		-DUSE_HTTPS=OpenSSL \
		-DREGEX_BACKEND=pcre2 \
		-DUSE_BUNDLED_ZLIB=OFF \
		-DBUILD_TESTS=OFF \
		-DBUILD_SHARED_LIBS=OFF
	cmake --build build-static
}

check() {
	cd "$builddir"/build-shared
	# Test 'offline' fails on s390x, aarch64 and ppc64le
	ctest -E 'offline|online' --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build-static
	DESTDIR="$pkgdir" cmake --install build-shared

	mkdir -p "$pkgdir"/usr/src/$pkgname/
	cp -a tests "$pkgdir"/usr/src/$pkgname/tests
}

tools() {
	pkgdesc="$pkgdesc (tools)"

	amove usr/bin
}

tests() {
	pkgdesc="$pkgdesc (tests)"

	amove usr/src/$pkgname
}

sha512sums="
68c8ed289de7daccaec17ea2ac49f4610325595cf90cddef763a31546a0a1c6bd400bf6180b68e2d3a8bdc3d031328efbbbaf3b61467dfc1b944e8cf3efcfd69  libgit2-1.7.0.tar.gz
de4485148cd7bb4a533e41764a8ff67d1afd77fd19b785ef6f880ae3c1f00a57375d411e5964305a223d5fffc612a462297f9c37f98f54f7eef977352359fc7e  libgit2-fix-grafts.patch
e2409f8e446c5742099787dedae12c5f4933ca95f3841e7ee571b85d6a862645defcfd38bb7b6607ff6e9f25544b9d5e0bb434aad0a7af6a2d22dcf27f2d29c7  fix-util-tests.patch
bb590e17f8c1eb379d72c29ea26e91cb7a2b860c556517d37e9a9374609a22b9ddbf65c770be9029b7bda4c2e9d7e8c7f6c08f4001b2866f4591b0ff3d502284  missing-header.patch
0d4e3aa518a0a114b7b57244c5968c0d43d3ffc90e9b605e72886288a7d42b3a63d9665229e37ba2176d82f29f2eda8d40aba990c9b8688212d2b8c68465e1fc  no-online-test.patch
"
