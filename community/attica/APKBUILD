# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=attica
pkgver=5.108.0
pkgrel=1
pkgdesc="Freedesktop OCS binding for Qt"
url="https://www.kde.org/"
arch="all !armhf" # Blocked by extra-cmake-modules
license="LGPL-2.0-or-later"
makedepends="
	doxygen
	extra-cmake-modules
	qt5-qtbase-dev
	qt5-qttools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc"
_repo_url="https://invent.kde.org/frameworks/attica.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/attica-$pkgver.tar.xz"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/attica.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# providertest requires network access
	ctest --test-dir build --output-on-failure -E "providertest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
38a28b03909e53399157cef9341c73b92bc129c65c0de61c39c9fca6133652cf8a689a9c1d32ff16f967e0ed6bd21183153b733843a78a0f7fa8b491c04f840f  attica-5.108.0.tar.xz
"
