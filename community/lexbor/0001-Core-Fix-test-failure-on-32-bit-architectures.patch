From 5cd5c2ccfac13da467657e0ef0d3bd33f45250d9 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?S=C3=B6ren=20Tempel?= <soeren+git@soeren-tempel.net>
Date: Sun, 6 Aug 2023 00:16:48 +0200
Subject: [PATCH] Core: Fix test failure on 32-bit architectures

On 32-bit architectures, the lexbor_core_hash test can segfault. This
has been reported in 2022 in #153. The root cause of the issue is that
the hash map implementation assumes that the hash map entry type is at
least large enough to store a `lexbor_hash_entry_t` type. This is never
explicitly documented anywhere, but the following allocation illustrates
this implicit assumption:

https://github.com/lexbor/lexbor/blob/743353b511bd554daf50c9e08945ee6790333157/source/lexbor/core/hash.c#L98

The `hash->entries` members corresponds to the `struct_size` specified
during `lexbor_hash_init`. Regarding `struct_size` the following
assumption must hold for the code above to make some sense:

	struct_size >= sizeof(lexbor_hash_entry_t)

For the `hash_entry_t` from `test/lexbor/core/hash.c` this assumption
does not necessarily hold as, contrary to other hash structures defined
in the source, this struct doesn't include a `lexbor_hash_entry_t`
member. This commit fixes this by adding this member.

Fixes #153
---
 test/lexbor/core/hash.c | 2 ++
 1 file changed, 2 insertions(+)

diff --git a/test/lexbor/core/hash.c b/test/lexbor/core/hash.c
index 583f3ea..bdd6a4e 100755
--- a/test/lexbor/core/hash.c
+++ b/test/lexbor/core/hash.c
@@ -10,6 +10,8 @@
 
 
 typedef struct {
+    lexbor_hash_entry_t entry;
+
     lexbor_hash_t hash;
     size_t        value;
 }
