# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=breeze-gtk
pkgver=5.27.7
pkgrel=1
pkgdesc="A GTK Theme Built to Match KDE's Breeze"
# armhf blocked by extra-cmake-modules
arch="noarch !armhf"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-only"
depends="gtk-engines"
makedepends="
	breeze
	breeze-dev
	extra-cmake-modules
	py3-cairo
	samurai
	sassc
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/breeze-gtk.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/breeze-gtk-$pkgver.tar.xz"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/breeze-gtk.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
68c868267bf6f85149015e2ecd5ef362994f6e632a4b28286d6488cecd2effa4792f4b17f1e1ceedc10e6ab4cfefb1380506302879e979b0f1daeefeda5fb454  breeze-gtk-5.27.7.tar.xz
"
