# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kbackup
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.kbackup"
pkgdesc="An application which lets you back up your data in a simple, user friendly way"
license="GPL-2.0-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libarchive-dev
	qt5-qtbase-dev
	samurai
	shared-mime-info
	"
_repo_url="https://invent.kde.org/utilities/kbackup.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kbackup-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6477b4b780368a706e347db4d8ee595df745b27cf99cf6924350f65a58a5d131a64a24e904d211441ea9ceb244c8d2e7282c358a6d569b1a625e0396a91a8e32  kbackup-23.04.3.tar.xz
"
